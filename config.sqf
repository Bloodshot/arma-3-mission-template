missionTitle = "Template";

squadRadio = "ACRE_PRC343";
platoonRadio = "ACRE_PRC152";

friendlySides = [west];
enemySide = east;

// Who can deploy at mission start?

// none: nobody
// leaders: group leaders deploys their team
// once: the first leader deploys everyone

deployAction = "leaders";

// Where do you deploy?

// default: deploy by clicking on the map
// marker: deploy automatically to a preset marker "deployMarker"
//         or to "{GROUP}Deploy" where {GROUP} is the groupId of the group
//         with spaces replaced by underscores

deployDestination = "default";

// What happens when you deploy?

// default: teleport to the destination
// boat: spawn and fill boats at the destination
// helicopter: spawn a helicopter and land at the destination
// paradrop: static line paradrop at the destination from a plane

deployMethod = "default";

// When should the mission start?

// deploy (default): after any deploy that leaves homeArea with no players
// auto: when there are no players left in homeArea
// manual: the mission framework will not automatically start the mission
// spawn: start the mission when Zeus spawns enemies
startMissionOn = "deploy";

friendlyPlaneClass = "RHS_C130J";
friendlyHeloClass = "RHS_CH_47F";
friendlyBoatClass = "I_G_Boat_Transport_01_F";

enemyParatrooperVehicle = "RHS_Mi8T_vdv";

aiSkill = 0.3;

// How much extra dispersion to add to enemy vehicle's weapons?
// Also effects infantry explosive weapons, such as UGLs and AT
aiVehicleDispersion = 0.35;

armorFactor = 1;

// What vehicle types to look for when creating vehicle pools
// Only the default types have any special behaviour (by default),
// but admin commands are created for all types
vehicleTypes = [
  "tank",
  "truck",
  "av",
  "apc",
  "ifv"
];

crateMap = [
  "FIRETEAM",
  "MMG",
  "DEMO"
];

supplyDropCrates = [
  "FIRETEAM",
  "FIRETEAM",
  "FIRETEAM",
  "FIRETEAM",
  "MMG",
  "MMG"
];

launcherSpawnChance =   1 / 2;

itemDropChance =        1 / 6;
medicalDropChance =     1 / 3;
aceDropChance =         1 / 6;

buildingGarrisonRatio = 1 / 5;
