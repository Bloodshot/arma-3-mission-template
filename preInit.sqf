#include "\x\rc\addons\mf\script_macros_mission.hpp"

GVAR(useFramework) = true;

if (!isClass (configFile >> "CfgPatches" >> "RC_MF")) exitWith {
  ["Rainbow Coalition Mission Framework not loaded; mission will not function."] call BIS_fnc_error;
};

waitUntil { missionNamespace getVariable ["rc_mf", false] };

[] call compile preprocessFileLineNumbers "config.sqf";
[] call compile preprocessFileLineNumbers "shared.sqf";

if (isServer) then {
  [] call compile preprocessFileLineNumbers "server.sqf";
};

if (hasInterface) then {
  [] call compile preprocessFileLineNumbers "client.sqf";
};
