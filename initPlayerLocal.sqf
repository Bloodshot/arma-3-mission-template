#include "\x\rc\addons\mf\script_macros_mission.hpp"

if (fileExists "crates.sqf") then {
  [[] call compile preprocessFileLineNumbers "crates.sqf"] call FUNC(addCrates);
};

if (fileExists "loadouts.sqf") then {
  GVAR(allowedArsenalItems) = [] call compile preprocessFileLineNumbers "loadouts.sqf";
};

if (fileExists "global.sqf") then {
  GVAR(globalArsenalItems) = [] call compile preprocessFileLineNumbers "global.sqf";
};

[] call FUNC(centerMapOnAO);
[] call FUNC(initRespawnSpectator);

waitUntil { !isNull player };

private _briefingRecords = [
  ["briefing\situation",  "Situation"],
  ["briefing\mission",    "Mission"],
  ["briefing\execution",  "Execution"]
];
["Diary", "Briefing", _briefingRecords] call FUNC(addDiary);
player selectDiarySubject (format ["Diary:Record%1", count _briefingRecords - 1]);

[] spawn FUNC(setACREChannels);
[] call FUNC(setRadarIcon);
[] call FUNC(addResetAction);
[] call FUNC(addArsenalAction);
[group player] call FUNC(initGroupEHs);

if (player == leader (group player)) then {
  if (didJIP) then {
    [] call FUNC(addJIPDeployAction);
  } else {
    if (deployAction == "leaders") then {
      [] call FUNC(addDeployAction);
    };
  };
} else {
  if !([leader group player] call FUNC(isHome)) then {
    [] call FUNC(addJIPDeployAction);
  };
};

[] call FUNC(initPlayerSettings);

if (isMultiplayer) then {
  [FUNC(updateBFTMarkers), 0.5] call CBA_fnc_addPerFrameHandler;
};

[] call FUNC(initWaypointTracker);
[] call FUNC(initKeybinds);
[] call FUNC(addRespawnEH);

[QGVAR(zeusCreated), {
  params ["_zeus"];
  if (is3DEN || is3DENMultiplayer || { [] call BIS_fnc_admin > 0 }) then {
    player assignCurator _zeus;
  };
}] call CBA_fnc_addEventHandler;

if (!isNil QGVAR(zeus) && { is3DEN || is3DENMultiplayer || { [] call BIS_fnc_admin > 0 }}) then {
  player assignCurator GVAR(zeus);
};
