#include "\x\rc\addons\mf\script_macros_mission.hpp"

if (fileExists "crates.sqf") then {
  [[] call compile preprocessFileLineNumbers "crates.sqf"] call FUNC(addCrates);
};

[] call FUNC(createZeus);
[] call FUNC(initTemplates);
[] call FUNC(initAiDeathInventory);
[] call FUNC(emptyAllVehicleCargo);
[] call FUNC(muteAllUnits);
[] call FUNC(haltEditorUnits);
[] call FUNC(populateEnemies);
[] call FUNC(aiFlashlightsOff);
[] call FUNC(initDynamicSimulation);
